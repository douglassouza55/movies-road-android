package com.abreu.moviesroad.data.entity;

public class MovieEntity {
    private long id;
    private String title;
    private String coverImageUrl;

    public long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getCoverImageUrl() {
        return coverImageUrl;
    }
}
